# Purpose

This terminal command is intended for two main purposes:

*   Quickly download IAGA 2002 formatted magnetic anomaly data
*   Convert IAGA 2002 text files in various reference frames to a consistent
    $`X`$, $`Y`$, $`Z`$, and $`F`$ frame and save it as a row-major,
    double-precision, IEEE floating-point binary file.

For a somewhat lacking explanation of the IAGA 2002 format, see [NOAA, IAGA 2002
Data Exchange
Format](https://www.ncei.noaa.gov/services/world-data-system/v-dat-working-group/iaga-2002-data-exchange-format).

# Formats

The IAGA 2002 format provides for three possible vector reference frames:
$`DHZ`$, $`DHI`$, and $`XYZ`$. The $`H`$ means "horizontal", which refers to the
horizontal component of the magnetic anomaly vector (the component orthogonal to
the downward axis as defined by the geodetic frame). The $`D`$ stands for
"declination", which is the angle relative to north in a clock-wise direction,
in [arc minutes](https://en.wikipedia.org/wiki/Minute_and_second_of_arc) (60ths
of a degree). The $`I`$ stands for "inclination", which is the angle relative to
horizontal in a downward direction, also in arc minutes. The $`XYZ`$ frame is
the North-East-Down, local-level (as defined by the geodetic frame) reference
frame. This terminal program will convert all vector readings to the $`XYZ`$
frame when saving the data to binary.

The IAGA 2002 format also provides for two methods of representing the scalar
magnetic anomaly reading: as whole values, $`F`$, or as differences, $`G`$,
relative to the vector norm of the vector reading. Unfortunately, it has been
verified that in some cases, data which is labeled as $`G`$ is in fact whole
values. This program will automatically save the scalar component to whole
values, ignoring the $`G`$ label if values exceed 1000 nT in magnitude, when
saving to binary.

In the IAGA 2002 format, the time of a record is written as the Gregorian
calendar date and the 24-hour time (apparently in the UTC time zone). When
converting to binary, this program will convert the time to Unix time in
seconds.

The binary output file, will be row-major with the following 5 columns of data:

1.  Time as Unix time in seconds
2.  $`X`$-axis coordinate of the vector sensor
3.  $`Y`$-axis coordinate of the vector sensor
4.  $`Z`$-axis coordinate of the vector sensor
5.  $`F`$ whole-value of the scalar sensor

All values are in floating-point format.

# Compilation

To compile the program, run the following in your terminal emulator:

```bash
cc -o iaga -lcurl iaga.c
```

Note that this program depends on the `libcurl` library. If your compiler does
not recognize `curl` you might need to install `libcurl`. If your compiler does
not automatically link the math library, you may need to do this manually:

```bash
cc -o iaga -lm -lcurl iaga.c
```

# Commands

This program downloads data from [IMAG-Data](https://imag-data.bgs.ac.uk/GIN/).
To download data, you need to know the code for the particular station location.
You can use the interactive map provided at
[Intermagnet.org](http://intermagnet.org/metadata/#/map). You also need to pick
a start date and an end data. The terminal command to download data would be
something like this (for Tucson):

```bash
iaga -l tuc -b 2010-01-01 -e 2010-01-31
```

This would download all the data at TUC (Tucson) from January 1, 2010 to January
31, 2010. At the moment, this program will only download the one-minute
sampling-rate data, not the one-second sampling-rate data.

Converting the IAGA 2002 formatted files to binary provides three main
advantages: (1) the data is much faster to read into other scripts, (2) no
special libraries are required to read the binary file, and (3) the data is
guaranteed to be in a consistent reference frame. To convert the downloaded
files to a single binary file, run a command like this:

```bash
iaga -o tuc.bin *.min
```

The IAGA 2002 files have a `.min` extension. The `*.min` is called a "glob" and
means any files with the ending `.min`. You could instead specify a specific
file (`TUC20100101qmin.min`) or all files that have `TUC` in them (`TUC*.min`).
The parameter `-o tuc.bin` is optional and specifies the name of the output
binary file. If this parameter is not specified, the default name will be used:
`out.bin`.

Naturally, the `-h` option will print out this help file.

# Binary File Import

How a binary file is read varies from language to language. In Python, this can
be done with the following code:

```python
data = np.fromfile('out.bin').reshape((-1, 5))
t = data[:, 0]
X = data[:, 1]
Y = data[:, 2]
Z = data[:, 3]
F = data[:, 4]
```

In MATLAB, this can be done with the following code:

```matlab
fid = fopen('frn.bin', 'rb');
data = fread(fid, [5, inf], 'double').';
fclose(fid);
t = data(:, 1);
X = data(:, 2);
Y = data(:, 3);
Z = data(:, 4);
F = data(:, 5);
```

# System-wide Access

To make the compiled program accessible from your whole system, move the
compiled binary program to someplace like `/usr/local/bin`. Actually, in the
terminal, enter the following command:

```bash
echo $PATH
```

This will list various directories, separated by colons (`:`), where you could
put the program.
