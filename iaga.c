/*  Copyright 2023 David Woodburn

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <math.h>
#include <curl/curl.h>

#define DEG_TO_RAD      0.01745329251994330  /* (rad/deg) pi/180 */
#define N_MAX_ACTIVE    32

/*  Convert Gregorian calendar dates (even prior to the introduction of the
    Gregorian calendar) to Julian Day Numbers. For a detailed explanation, see
    "Explanation of Julian Day Number Calculation" by W. Zhang.  */
int gregorian_to_jdn(int Y, int M, int D) {
    int a, y, m;
    a = (14 - M)/12; /* month adjustment */
    y = Y + 4800 - a; /* modified year */
    m = M + 12*a - 3; /* modified month */
    return D + (153*m + 2)/5 + 365*y +
        y/4 - y/100 + y/400 - 32045;
}

/*  Convert Julian Day Numbers to Gregorian calendar dates (even prior to the
    introduction of the Gregorian calendar). For a detailed explanation, see
    "Calendars" in Explanatory Supplement to the Astronomical Almanac, 3rd
    edition, pp. 617-619, by E. G. Richards, found at
    https://aa.usno.navy.mil/downloads/c15_usb_online.pdf. Using 32-bit ints,
    this algorithm along with the gregorian_to_jdn() function are perfect duals
    from Julian Day Number -1363 (March 1, -4716) to 536 802 342 (October 17, 1
    465 002). Furthermore, within that date range, each Julian Day Number is
    consecutive.  */
void jdn_to_gregorian(int J, int *Y, int *M, int *D) {
    int f, e, g, h;
    f = J + 1363 + (((4*J + 274277)/146097)*3)/4;
    e = 4*f + 3;
    g = (e % 1461)/4;
    h = 5*g + 2;
    *D = (h % 153)/5 + 1;
    *M = ((h/153 + 2) % 12) + 1;
    *Y = e/1461 - 4716 + (14 - *M)/12;
}

void progress_bar(unsigned long n, unsigned long N) {
    static time_t
        t_start = 0,
        t_last = 0;
    time_t t_now = 0;
    int m = 0, M = 60, w = 0;

    /*  Get the current time.  */
    if (n == 0) {
        /*  Initialize the timer.  */
        t_start = time(NULL);
        return;
    } else {
        /*  Exit early if not enough time has passed
            and it is not the end.  */
        t_now = time(NULL);
        if ((t_now - t_last < 1) && (n < N)) {
            return;
        }
        t_last = t_now;
    }

    /*  Get width of biggest number and adjust total width.  */
    w = floor(log10(N)) + 1;
    M -= 2*w + 4;

    /*  Print start of bar.  */
    printf("\r%*lu/%*lu [", w, n, w, N);

    /*  Print middle of bar.  */
    for (m = 0; m < M; m++) {
        if (m <= (n*M)/N) {
            printf("/");
        } else {
            printf("-");
        }
    }

    /*  Print end of bar.  */
    printf("] %ld", t_now - t_start);
    if (n == N) {
        printf("\n");
    }
    fflush(stdout);
}

void upper(char *src, char *dst) {
    size_t
        n = 0,
        len = 0;

    len = strlen(src);
    for (n = 0; n < len; n++) {
        if ((src[n] < 97) || (src[n] > 127)) {
            dst[n] = src[n];
        } else {
            dst[n] = src[n] - 32;
        }
    }
}

int str_to_date(char *str, int *year, int *month, int *day) {
    int n = 0;

    /*  Ensure the string is not NULL.  */
    if (str == NULL) {
        return -2;
    }

    /*  Check the length of the string.  */
    if (strlen(str) < 10) {
        return -1;
    }

    /*  Check the format of the string.  */
    for (n = 0; n < 10; n++) {
        if ((n == 4) || (n == 7)) {
            if (str[n] != '-') {
                return -1;
            }
            str[n] = '\0';
        } else {
            if ((str[n] < 48) || (str[n] > 57)) {
                return -1;
            }
        }
    }

    /*  Convert string to numbers.  */
    *year = atoi(str);
    *month = atoi(&str[5]);
    *day = atoi(&str[8]);
    if ((*year < 1) || (*month < 1) || (*month > 12) ||
            (*day < 1) || (*day > 31)) {
        return -1;
    }

    return 0;
}

void usage(void) {
    printf(
        "--------------------------------------------------------------------\n"
        "Purpose\n\n"
        "This terminal command is intended for two main purposes:\n\n"
        "    *   Quickly download IAGA 2002 formatted magnetic anomaly data\n"
        "    *   Convert IAGA 2002 text files in various reference frames to\n"
        "        a consistent `X`, `Y`, `Z`, and `F` frame and save it as a\n"
        "        row-major, double-precision, IEEE floating-point binary\n"
        "        file.\n\n"
        "For a somewhat lacking explanation of the IAGA 2002 format, see\n"
        "https://www.ncei.noaa.gov/services/world-data-system/\n"
        "v-dat-working-group/iaga-2002-data-exchange-format.\n\n"
        "--------------------------------------------------------------------\n"
        "Formats\n\n"
        "The IAGA 2002 format provides for three possible vector reference\n"
        "frames: `DHZ`, `DHI`, and `XYZ`. The `H` means \"horizontal\", which\n"
        "refers to the horizontal component of the magnetic anomaly vector\n"
        "(the component orthogonal to the downward axis as defined by the\n"
        "geodetic frame). The `D` stands for \"declination\", which is the\n"
        "angle relative to north in a clock-wise direction, in\n"
        "arc minutes (https://en.wikipedia.org/wiki/Minute_and_second_of_arc)\n"
        "(60ths of a degree). The `I` stands for \"inclination\", which is\n"
        "the angle relative to horizontal in a downward direction, also in\n"
        "arc minutes. The `XYZ` frame is the North-East-Down, local-level (as\n"
        "defined by the geodetic frame) reference frame. This terminal\n"
        "program will convert all vector readings to the `XYZ` frame when\n"
        "saving the data to binary.\n\n"
        "The IAGA 2002 format also provides for two methods of representing\n"
        "the scalar magnetic anomaly reading: as whole values, `F`, or as\n"
        "differences, `G`, relative to the vector norm of the vector reading.\n"
        "Unfortunately, it has been verified that in some cases, data which\n"
        "is labeled as `G` is in fact whole values. This program will\n"
        "automatically save the scalar component to whole values, ignoring\n"
        "the `G` label if values exceed 1000 nT in magnitude, when saving to\n"
        "binary.\n\n"
        "In the IAGA 2002 format, the time of a record is written as the\n"
        "Gregorian calendar date and the 24-hour time (apparently in the UTC\n"
        "time zone). When converting to binary, this program will convert the\n"
        "time to Unix time in seconds.\n\n"
        "The binary output file, will be row-major with the following 5\n"
        "columns of data:\n\n"
        "    1.  Time as Unix time in seconds\n"
        "    2.  `X`-axis coordinate of the vector sensor\n"
        "    3.  `Y`-axis coordinate of the vector sensor\n"
        "    4.  `Z`-axis coordinate of the vector sensor\n"
        "    5.  `F` whole-value of the scalar sensor\n\n"
        "All values are in floating-point format.\n\n"
        "--------------------------------------------------------------------\n"
        "Compilation\n\n"
        "To compile the program, run the following in your terminal "
        "emulator:\n\n    cc -o iaga -lcurl iaga.c\n\n"
        "Note that this program depends on the `libcurl` library. If your\n"
        "compiler does not recognize `curl` you might need to install\n"
        "`libcurl`. If your compiler does not automatically link the math\n"
        "library, you may need to do this manually:\n\n"
        "    cc -o iaga -lm -lcurl iaga.c\n\n"
        "--------------------------------------------------------------------\n"
        "Commands\n\n"
        "This program downloads data from https://imag-data.bgs.ac.uk/GIN/.\n"
        "To download data, you need to know the code for the particular\n"
        "station location. You can use the interactive map provided at\n"
        "http://intermagnet.org/metadata/#/map. You also need to pick a start\n"
        "date and an end data. The terminal command to download data would be\n"
        "something like this (for Tucson):\n\n"
        "    iaga -l tuc -b 2010-01-01 -e 2010-01-31\n\n"
        "This would download all the data at TUC (Tucson) from January 1,\n"
        "2010 to January 31, 2010. At the moment, this program will only\n"
        "download the one-minute sampling-rate data, not the one-second\n"
        "sampling-rate data.\n\n"
        "Converting the IAGA 2002 formatted files to binary provides three\n"
        "main advantages: (1) the data is much faster to read into other\n"
        "scripts, (2) no special libraries are required to read the binary\n"
        "file, and (3) the data is guaranteed to be in a consistent reference\n"
        "frame. To convert the downloaded files to a single binary file, run\n"
        "a command like this:\n\n"
        "    iaga -o tuc.bin *.min\n\n"
        "The IAGA 2002 files have a `.min` extension. The `*.min` is called a\n"
        "\"glob\" and means any files with the ending `.min`. You could\n"
        "instead specify a specific file (`TUC20100101qmin.min`) or all files\n"
        "that have `TUC` in them (`TUC*.min`). The parameter `-o tuc.bin` is\n"
        "optional and specifies the name of the output binary file. If this\n"
        "parameter is not specified, the default name will be used:\n"
        "`out.bin`.\n\n"
        "Naturally, the `-h` option will print out this help file.\n\n"
        "--------------------------------------------------------------------\n"
        "Binary File Import\n\n"
        "How a binary file is read varies from language to language. In\n"
        "Python, this can be done with the following code:\n\n"
        "    data = np.fromfile('out.bin').reshape((-1, 5))\n"
        "    t = data[:, 0]\n"
        "    X = data[:, 1]\n"
        "    Y = data[:, 2]\n"
        "    Z = data[:, 3]\n"
        "    F = data[:, 4]\n\n"
        "In MATLAB, this can be done with the following code:\n\n"
        "    fid = fopen('frn.bin', 'rb');\n"
        "    data = fread(fid, [5, inf], 'double').';\n"
        "    fclose(fid);\n"
        "    t = data(:, 1);\n"
        "    X = data(:, 2);\n"
        "    Y = data(:, 3);\n"
        "    Z = data(:, 4);\n"
        "    F = data(:, 5);\n\n"
        "--------------------------------------------------------------------\n"
        "System-wide Access\n\n"
        "To make the compiled program accessible from your whole system, move\n"
        "the compiled binary program to someplace like `/usr/local/bin`.\n"
        "Actually, in the terminal, enter the following command:\n\n"
        "    echo $PATH\n\n"
        "This will list various directories, separated by colons (`:`), where\n"
        "you could put the program.\n");
}

int convert_file(FILE* iaga_file, FILE* bin_file) {
    char
        *line = NULL,
        frame_str[5] = "    ";
    int
        n = 0,
        frame_check = 0x0,
        frame = 0, /* 0: XYZ, 3: DHZ, 7: DHI */
        is_g = 0,
        n_xd = 0, n_yh = 0, n_zi = 0, n_fg = 0,
        n_word_start = 0,
        word_n = 0,
        in_word = 0,
        channels_used = 0,
        channel_mask[4] = {1, 2, 4, 8},
        jdn = 0,
        year = 0,
        month = 0,
        day = 0;
    size_t len = 0;
    double
        x = 0.0, y = 0.0, z = 0.0,
        d = 0.0, h = 0.0, i = 0.0,
        f = 0.0, g = 0.0,
        line_data[5] = {0.0};

    /*  Read the first line, in case curl had an error.  */
    getline(&line, &len, iaga_file);

    /*  Find the header line and interpret the frame.  */
    while (getline(&line, &len, iaga_file) != -1) {
        if (line[0] != ' ') {
            /*  Ensure the first word is "DATE"
                and the line is at least 70 characters.  */
            if ((len < 70) || (strncmp(line, "DATE", 4) != 0)) {
                fprintf(stderr, "File does not appear to have a header.\n");
                return 0x0;
            }

            /*  Extract the frame.  */
            n = 5;
            while (n < 67) {
                if (line[n] == ' ') {
                    n++;
                } else {
                    word_n++;
                    if ((word_n >= 3) && (word_n <= 6)) {
                        frame_str[word_n - 3] = tolower(line[n + 3]);
                        switch (frame_str[word_n - 3]) {
                        case 'x':
                            frame_check |= 0x1;
                            frame &= ~0x1;
                            n_xd = word_n - 2;
                            break;
                        case 'y':
                            frame_check |= 0x2;
                            frame &= ~0x2;
                            n_yh = word_n - 2;
                            break;
                        case 'z':
                            frame_check |= 0x4;
                            frame &= ~0x4;
                            n_zi = word_n - 2;
                            break;
                        case 'd':
                            frame_check |= 0x1;
                            frame |= 0x1;
                            n_xd = word_n - 2;
                            break;
                        case 'h':
                            frame_check |= 0x2;
                            frame |= 0x2;
                            n_yh = word_n - 2;
                            break;
                        case 'i':
                            frame_check |= 0x4;
                            frame |= 0x4;
                            n_zi = word_n - 2;
                            break;
                        case 'f':
                            frame_check |= 0x8;
                            n_fg = word_n - 2;
                            is_g = 0;
                            break;
                        case 'g':
                            frame_check |= 0x8;
                            n_fg = word_n - 2;
                            is_g = 1;
                            break;
                        default:
                            ;
                        }
                    }
                    n += 4;
                }
            }

            /*  Ensure the frame is fully-defined and valid.  */
            if (frame_check != 0xF) {
                fprintf(stderr, "\rFrame is under-defined: %s!\n", frame_str);
                return 0x0;
            }
            if ((frame != 0x0) && (frame != 0x3) && (frame != 0x7)) {
                fprintf(stderr, "\rFrame is invalid: %s!\n", frame_str);
                return 0x0;
            }
            break;
        }
    }

    /*  Make sure a frame was found.  */
    if (frame_check == 0x0) {
        fprintf(stderr, "\rThe file could not be found "
            "or does not specify a frame!\n");
        return 0x0;
    }

    // FIXME Remove.
    if (frame == 0x7) {
        printf("\rDHI frame!\n");
    }

    /*  Process the values in the file.  */
    while (getline(&line, &len, iaga_file) != -1) {
        /*  Process the values in one line.  */
        word_n = 0;
        in_word = 0;
        for (n = 0; n <= 70; n++) {
            /*  Find beginning and ending of a word.  */
            if (in_word) {
                if ((line[n] == ' ') || (n == 70)) {
                    line[n] = '\0';
                    in_word = 0;
                }
            } else {
                if (line[n] != ' ') {
                    in_word = 1;
                    n_word_start = n;
                }
            }

            /*  Process the word.  */
            if (line[n] == '\0') {
                if (word_n == 0) { /* date */
                    /*  Insert nulls.  */
                    line[n_word_start + 4] = '\0';
                    line[n_word_start + 7] = '\0';

                    /*  Get year, month, and day.  */
                    year = atoi(&line[n_word_start]);
                    month = atoi(&line[n_word_start + 5]);
                    day = atoi(&line[n_word_start + 8]);

                    /*  Convert to Unix time.  */
                    jdn = gregorian_to_jdn(year, month, day);
                    line_data[0] = (double)(jdn - 2440588)*86400.0;
                } else if (word_n == 1) { /* time */
                    /*  Insert nulls.  */
                    line[n_word_start + 2] = '\0';
                    line[n_word_start + 5] = '\0';

                    /*  Add hours, minutes, and seconds.  */
                    line_data[0] += 3600.0*atoi(&line[n_word_start]);
                    line_data[0] += 60.0*atoi(&line[n_word_start + 3]);
                    line_data[0] +=
                        strtod(&line[n_word_start + 6], (char **)NULL);
                } else if (word_n >= 3) { /* coordinates */
                    /*  Convert string coordinate to a number.  */
                    line_data[word_n - 2] =
                        strtod(&line[n_word_start], (char **)NULL);
                    if (line_data[word_n - 2] > 88887) {
                        line_data[word_n - 2] = NAN;
                    } else {
                        /*  Note that the channel has data.  */
                        channels_used |= channel_mask[word_n - 3];
                    }

                    /*  Save the coordinates and stop processing the line
                        after word 6.  */
                    if (word_n == 6) {
                        /*  Get the vector in xyz frame.  */
                        switch (frame) {
                        case 0x0: /* xyz set */
                            x = line_data[n_xd];
                            y = line_data[n_yh];
                            z = line_data[n_zi];
                            break;
                        case 0x3: /* dhz set */
                            d = DEG_TO_RAD*line_data[n_xd]/60.0;
                            h = line_data[n_yh];
                            z = line_data[n_zi];
                            x = h*cos(d);
                            y = h*sin(d);
                            break;
                        case 0x7: /* dhi set */
                            d = DEG_TO_RAD*line_data[n_xd]/60.0;
                            h = line_data[n_yh];
                            i = DEG_TO_RAD*line_data[n_zi]/60.0;
                            x = h*cos(d);
                            y = h*sin(d);
                            z = h*tan(i);
                        }

                        /*  Correct F or G designation.  */
                        if (is_g && (line_data[n_fg] > 1000.0)) {
                            is_g = 0;
                        } else if (!is_g && (line_data[n_fg] < 1000.0)) {
                            is_g = 1;
                        }

                        /*  Get the absolute scalar.  */
                        if (is_g) {
                            f = sqrt(x*x + y*y + z*z) - line_data[n_fg];
                        } else {
                            f = line_data[n_fg];
                        }

                        /*  Store the coordinates in the array.  */
                        line_data[1] = x;
                        line_data[2] = y;
                        line_data[3] = z;
                        line_data[4] = f;

                        /*  Write the data to the binary file.  */
                        fwrite(line_data, sizeof (double), 5, bin_file);
                        break;
                    }
                }
                word_n++;
            }
        }
    }

    /*  Free the line.  */
    if (line) {
        free(line);
    }

    return channels_used;
}

static size_t write_iaga(void* contents, size_t size, size_t len,
        void* stream) {
    size_t bytes = 0;
    FILE* file = (FILE*)stream;
    bytes = fwrite(contents, size, len, file);
    return bytes;
}

int main(int argc, char *argv[]) {
    char
        url[225] = "https://imag-data.bgs.ac.uk/GIN_V1/GINServices?"
            "Request=GetData&format=IAGA2002&testObsys=0&observatoryIagaCode="
            "xxx&samplesPerDay=1440&publicationState="
            "adj-or-rep&recordTermination=UNIX&dataStartDate=xxxx-xx-xx"
            "&dataDuration=1",
        *bin_str = NULL,
        code_str[4] = "",
        date_str[11] = "",
        out_str[20] = "cccddddddddqmin.min";
    int
        opt = 0,
        target_flag = 0,
        bin_len = 0,
        n = 0,
        N_active = 0,
        jdn_begin = 0,
        jdn_end = 0,
        jdn = 0,
        year = 0,
        month = 0,
        day = 0,
        N_days_done = 0,
        N_days_total = 0,
        still_running = 0,
        numfds = 0,
        curl_msgs_left = 0;
    FILE
        *out_file = NULL,
        *iaga_file = NULL;
    CURL *curl = NULL;
    CURL *curls[N_MAX_ACTIVE] = {NULL};
    CURLM *multi_curl = NULL;
    CURLcode curl_code;
    CURLMcode curlm_code;
    CURLMsg *curlm_msg = NULL;

    /*  Process the input options.  */
    while ((opt = getopt(argc, argv, "hl:b:e:o:")) != -1) {
        switch (opt) {
        case 'h':
            usage();
            break;
        case 'l':
            target_flag |= 0x1;
            if (strlen(optarg) != 3) {
                fprintf(stderr, "The location code should be three letters!\n");
                return EXIT_FAILURE;
            }
            strcpy(code_str, optarg);
            upper(code_str, code_str);
            break;
        case 'b':
            target_flag |= 0x2;
            if (strlen(optarg) != 10 ||
                    str_to_date(optarg, &year, &month, &day)) {
                fprintf(stderr,
                    "The begin date has the wrong format: yyyy-mm-dd.\n");
                return EXIT_FAILURE;
            }
            jdn_begin = gregorian_to_jdn(year, month, day);
            jdn = jdn_begin;
            break;
        case 'e':
            target_flag |= 0x4;
            if (strlen(optarg) != 10 ||
                    str_to_date(optarg, &year, &month, &day)) {
                fprintf(stderr,
                    "The end date has the wrong format: yyyy-mm-dd.\n");
                return EXIT_FAILURE;
            }
            jdn_end = gregorian_to_jdn(year, month, day);
            break;
        case 'o':
            bin_len = strlen(optarg);
            bin_str = malloc(bin_len + 1);
            if (bin_str == NULL) {
                fprintf(stderr, "Could not allocate memory for a string!\n");
                exit(1);
            }
            strncpy(bin_str, optarg, bin_len + 1);
            break;
        case '?':
        default:
            fprintf(stderr, "Unrecognized flags!\n");
            return EXIT_FAILURE;
        }
    }
    argc -= optind;
    argv += optind;

    /*  --------------------
        Download IAGA files.
        --------------------  */

    if (target_flag != 0x0) {
        /*  Ensure the location code, begin date, and
            end date are all specified.  */
        if (target_flag != 0x7) {
            fprintf(stderr, "The location code, begin date, and end date "
                "all must be given!\n");
            return EXIT_FAILURE;
        }

        /*  Initialize libcurl.  */
        if (curl_global_init(CURL_GLOBAL_ALL)) {
            fprintf(stderr, "Could not initialize libcurl!\n");
            return EXIT_FAILURE;
        }

        /*  Initialize the multi handle.  */
        multi_curl = curl_multi_init();
        if (multi_curl == NULL) {
            fprintf(stderr, "Could not initialize multi handle!\n");
            return EXIT_FAILURE;
        }

        /*  Initialize the progress bar.  */
        N_days_total = jdn_end - jdn_begin + 1;
        progress_bar(0, N_days_total);

        /*  Load the pipeline.  */
        for (N_active = 0; N_active < N_MAX_ACTIVE; N_active++) {
            /*  Get the Gregorian date.  */
            jdn_to_gregorian(jdn, &year, &month, &day);

            /*  Insert the location code and date to the URL.  */
            memcpy(&url[111], code_str, 3);
            snprintf(date_str, 11, "%04d-%02d-%02d", year, month, day);
            memcpy(&url[199], date_str, 10);

            /*  Insert the location code and date to the file name.  */
            snprintf(out_str, 20, "%s%04d%02d%02dqmin.min",
                code_str, year, month, day);

            /*  Open the output file.  */
            out_file = fopen(out_str, "wb");
            if (out_file == NULL) {
                fprintf(stderr, "Failed to open output file.\n");
                return EXIT_FAILURE;
            }

            /*  Initialize the curl handles.  */
            curls[N_active] = curl_easy_init();
            if (curls[N_active] == NULL) {
                fprintf(stderr, "Failed to initialize easy curl!\n");
                return EXIT_FAILURE;
            }

            /*  Define handle options.  */
            curl_easy_setopt(curls[N_active], CURLOPT_WRITEFUNCTION,
                write_iaga);
            curl_easy_setopt(curls[N_active], CURLOPT_URL, url);
            curl_easy_setopt(curls[N_active], CURLOPT_WRITEDATA, out_file);
            curl_easy_setopt(curls[N_active], CURLOPT_PRIVATE, out_file);

            /*  Add the handle to the multi handle.  */
            curl_multi_add_handle(multi_curl, curls[N_active]);

            /*  Increment the Julian Day Number.  */
            jdn++;
            if (jdn > jdn_end) {
                N_active++;
                break;
            }
        }

        /*  Download files.  */
        do {
            /*  Manage downloads.  */
            curlm_code = curl_multi_perform(multi_curl, &still_running);
            if (curlm_code != CURLM_OK) {
                fprintf(stderr, "Downloads group failed: %s\n",
                    curl_multi_strerror(curlm_code));
                return EXIT_FAILURE;
            }

            /*  Wait for activity.  */
            curlm_code = curl_multi_wait(multi_curl, NULL, 0, 30*1000, &numfds);
            if (curlm_code != CURLM_OK) {
                fprintf(stderr, "Downloads group failed: %s\n",
                    curl_multi_strerror(curlm_code));
                return EXIT_FAILURE;
            }

            /*  Manage completed downloads by reading the messages.  */
            while ((curlm_msg =
                    curl_multi_info_read(multi_curl, &curl_msgs_left))) {
                if (curlm_msg->msg == CURLMSG_DONE) {
                    /*  Check if the individual download failed.  */
                    curl_code = curlm_msg->data.result;
                    if (curl_code != CURLE_OK) {
                        curl_easy_getinfo(curl, CURLINFO_EFFECTIVE_URL, &url);
                        snprintf(date_str, 11, "%s", &url[199]);
                        fprintf(stderr,
                            "Single download failed: (%d) %s for %s\n",
                            curl_code, curl_easy_strerror(curl_code), date_str);
                        continue;
                    }

                    /*  Mark day as completed.  */
                    N_days_done++;
                    progress_bar(N_days_done, N_days_total);

                    /*  Remove the handle from the multi handle.  */
                    curl = curlm_msg->easy_handle;
                    curl_multi_remove_handle(multi_curl, curl);
                    curl_easy_getinfo(curl, CURLINFO_PRIVATE, &out_file);
                    fclose(out_file);

                    /*  Add another download to the pipeline.  */
                    if (jdn <= jdn_end) {
                        /*  Get the Gregorian date.  */
                        jdn_to_gregorian(jdn, &year, &month, &day);

                        /*  Insert the location code and date to the URL.  */
                        memcpy(&url[111], code_str, 3);
                        snprintf(date_str, 11, "%04d-%02d-%02d",
                            year, month, day);
                        memcpy(&url[199], date_str, 10);

                        /*  Write the output file name.  */
                        snprintf(out_str, 20, "%s%04d%02d%02dqmin.min",
                            code_str, year, month, day);

                        /*  Open the output file.  */
                        out_file = fopen(out_str, "wb");
                        if (out_file == NULL) {
                            fprintf(stderr, "Failed to open output file.\n");
                            return EXIT_FAILURE;
                        }

                        /*  Define handle options.  */
                        curl_easy_setopt(curl, CURLOPT_URL, url);
                        curl_easy_setopt(curl, CURLOPT_WRITEDATA, out_file);
                        curl_easy_setopt(curl, CURLOPT_PRIVATE, out_file);

                        /*  Add the handle to the multi handle.  */
                        curl_multi_add_handle(multi_curl, curl);

                        /*  Increment the Julian Day Number.  */
                        jdn++;
                    }
                }
            }
        } while (still_running || (jdn <= jdn_end));

        /*  Clean up all the easy handles.  */
        for (n = 0; n < N_active; n++) {
            curl_easy_cleanup(curls[n]);
        }

        /*  Clean up the multi handle.  */
        curl_multi_cleanup(multi_curl);

        /*  Clean up libcurl.  */
        curl_global_cleanup();
    }

    /*  ------------------------
        Convert files to binary.
        ------------------------  */

    /*  Ensure some files were specified.  */
    if (argc > 0) {
        /*  Open the binary file.  */
        if (bin_len > 0) {
            out_file = fopen(bin_str, "wb");
        } else {
            out_file = fopen("out.bin", "wb");
        }
        if (out_file == NULL) {
            fprintf(stderr,"Could not open output binary file!\n");
            exit(1);
        }

        /*  Initialize the progress bar.  */
        progress_bar(0, 0);

        /*  Convert each file.  */
        for (n = 0; n < argc; n++) {
            /*  Open a single file.  */
            iaga_file = fopen(argv[n], "r");
            if (iaga_file == NULL) {
                fprintf(stderr, "Could not open file %s!\n", argv[n]);
                continue;
            }

            /*  Convert the file, appending the result to the binary file.  */
            convert_file(iaga_file, out_file);

            /*  Close the IAGA file.  */
            fclose(iaga_file);

            /*  Update the progress bar.  */
            progress_bar(n + 1, argc);
        }

        /*  Close the binary file.  */
        fclose(out_file);
    }

    return EXIT_SUCCESS;
}
